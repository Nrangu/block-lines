﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library;
namespace Match3 { 

    public class GameMath3:Game
    {
        #region private fields
        static GameMath3 _ref = null;
        Main _mainObject = null;
        #endregion
        #region private methods
        
        GameMath3()
        {
        }
        #endregion
        #region public methods
        public static GameMath3 Instace()
        {
            if ( _ref == null)
            {
                _ref = new GameMath3();
            }
            return _ref;
        }
        public void GameOver()
        {
            State = GameState.GAMEOVER;
        }
        public void Start()
        {
            State = GameState.START;
        }
        public void Play()
        {
            State = GameState.PLAY;
        }
        public bool IsPlay
        {
            get
            {
                return State == GameState.PLAY;
            }
        }
        #endregion
        #region public properties
        public Main Main
        {
            get
            {
                return _mainObject;
            }
            set
            {
                _mainObject = value;
            }
        }
        #endregion
    }
}

