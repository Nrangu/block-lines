﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Library;

namespace Match3
{
    public class CreateBlocks : MonoBehaviour
    {
        #region private fields
        [SerializeField]
        private GameObject[] _blocks;
        [SerializeField]
        private Transform _endPosition;
 
        [SerializeField]
        private Field _field;
        private float _tmpTime = 0;
        private GameMath3 _math3;
        #endregion
        #region public fields
        public float _timeCreate;
        public float _speed = 0.8f;
        public float _speedFly = 35f;
        //public Main _main;
        #endregion

        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            _tmpTime = _timeCreate;
            _math3 = GameMath3.Instace();
        }

        // Update is called once per frame
        void Update()
        {
            if ( !_math3.IsPlay)
            {
                return;
            }

            _tmpTime += Time.deltaTime;
            if(_tmpTime >= _timeCreate)
            {
                _tmpTime = 0;
                int i = Random.Range(0, _blocks.Length);
                GameObject tmpObj = Instantiate(_blocks[i]);
                tmpObj.transform.position = new Vector3(transform.position.x, transform.position.y, -1);
                tmpObj.transform.localScale = new Vector3(Main.GetMain.CellScale, Main.GetMain.CellScale);
                ColorBlock block = tmpObj.GetComponent<ColorBlock>();
                block._type = i;
                block._endPoint = _endPosition.position;
                block._speed = _speed;
                block._field = _field;
                block._math3 = _math3;
                block._flySpeed = _speedFly;
            }
        }
        #endregion
    }

}