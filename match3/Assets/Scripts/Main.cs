﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Library;
using UnityEngine.Advertisements;
using Advertising;

namespace Match3
{
    public class Main : MonoBehaviour
    {
        #region private fields
        [SerializeField]
        CreateBlocks _createBlocks;
        [SerializeField]
        float _timeCreate;
        [SerializeField]
        float _delaTimeCreate;

        [SerializeField]
        float _speedBlock;
        [SerializeField]
        float _deltaSpeedBlock;
        [SerializeField]
        int _countDeltas;
        [SerializeField]
        int _scoreStep;
        int _currentDelta = 0;
        int _scoreCondition = 0;

        static Main _ref;
        GameMath3 _math3;
        [SerializeField]
        Text _scoreFadeText;

        [SerializeField]
        Text _higherScoreText;

        [SerializeField]
        Canvas _canvas;

        [SerializeField]
        GameObject _GameOverMenu;

        [SerializeField]
        GameObject _scorePanel;

        [SerializeField]
        string _gameId = "123456";

        [SerializeField]
        bool _testMode = true;

        [SerializeField]
        string _placementId = "BannerPlacement";
        /*
        [SerializeField]
        float _margin = 25;
        */
        [SerializeField]
        int _widthInUnits=6;

        [SerializeField]
        int _colls = 5;

        [SerializeField]
        int _rows = 7;
        float _cellScale;
        [SerializeField]
        Transform _createBlockPosition;

        [SerializeField]
        Text _scoreText;
        int _score = 0;
        int _higherScore = 0;
        Ads _ads;
        #endregion

        #region private methods
        private void Awake()
        {
            if (_ref == null)
            {
                _ref = this;
            }
            else
            {
                Destroy(gameObject);
            }


            _math3 = GameMath3.Instace();
            _math3.Start();
            _math3.Main = this;
            _GameOverMenu.SetActive(false);

            _math3.gameOver += () => { PlayerPrefs.Save(); _GameOverMenu.SetActive(true); };
            _cellScale = (float)_widthInUnits / (float)_colls;
            Field field = GetComponent<Field>();

            field._colls = _colls;
            field._rows = _rows;

            field._cellSize = _cellScale;
            _ads = new Ads(_gameId, _testMode);
            _ads.PlacementId = _placementId;

            _higherScore = PlayerPrefs.GetInt("HigherScore", 0);
            _higherScoreText.text = _higherScore.ToString();

            _score = 0;
            _scoreText.text = _score.ToString();
            ConditionInit();

        }


        void Start()
        {
            StartCoroutine(_ads.ShowBannerWhenReady());

            Field field = GetComponent<Field>();
            



            field.CreateField();
        }

        // Update is called once per frame
        void Update()
        {
           if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }
            Condition();
        }
        private void Quit()
        {
            PlayerPrefs.Save();
        }
        private void OnApplicationQuit()
        {
            Quit();
        }
        private void OnApplicationPause(bool pause)
        {
            Quit();
        }
        private void ConditionInit()
        {
            _currentDelta = 1;
            _scoreCondition = _scoreStep;
            _createBlocks._timeCreate = _timeCreate;
            _createBlocks._speed = _speedBlock;
        }
        private void Condition()
        {
            if (_currentDelta > _countDeltas)
            {
                Debug.Log("Max speed");
                return;
            }
            if (_score > _currentDelta * _scoreStep)
            {
                _createBlocks._timeCreate += _delaTimeCreate;
                _createBlocks._speed += _deltaSpeedBlock;
                _currentDelta++;
                Debug.Log("Speed up!!!");
            }
        }
        #endregion
        #region public methods
        public void GamePlay()
        {
            _math3.Play();
        }
        #endregion
        #region public properties
        public float CellScale
        {
            get
            {
                return _cellScale;
            }
        }
        public GameMath3 GameMain
        {
            get
            {
                return _math3;
            }
        }
        #endregion
        #region public methods
        public void ShowAds()
        {
            Advertisement.Show();
            //
        }
        public void AddScore(int score_)
        {
            _score += score_;
            _scoreText.text = _score.ToString();

            if (_score > _higherScore)
            {
                _higherScore = _score;
                _higherScoreText.text = _higherScore.ToString();
                PlayerPrefs.SetInt("HigherScore", _higherScore);

            }
        }
        public void AddScore(int score_, float x_, float y_ )
        {

            //_score += score_;
            //_scoreText.text = _score.ToString();
            AddScore(score_);
            Text tmpText = Instantiate(_scoreFadeText, _canvas.transform);
            tmpText.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(new Vector2(x_, y_));
            tmpText.text = score_.ToString();
        }

        public static Main GetMain
        {
            get
            {
                return  _ref != null ?  _ref :  null;
            }
        }
        public Ads Ads
        {
            get
            {
                return _ads;
            }
        }
        #endregion

    }
}
