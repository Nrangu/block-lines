﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Match3
{

    public class ColorBlock : MonoBehaviour
    {
        #region public fields
        public Field _field;
        public int _type;
        public Vector3 _endPoint;
        public float _speed = 0.1f;
        public float _flySpeed = 100f;
        public ParticleSystem _fx;
        public Action blockStop = () => { };

        #endregion
        #region private fields
        public GameMath3 _math3;
        private bool _move = true;
        private bool _fly = false;
        private Vector3 _target;
        private Animator _compressedBlock;
        private float _animationDuration;
        #endregion
        #region private methods
        private void Awake()
        {
            _math3 = GameMath3.Instace();
            _compressedBlock = GetComponent<Animator>();

            blockStop += () =>
            {
 
                transform.position = _target;

                _fly = false;
                if (_compressedBlock != null)  _compressedBlock.SetBool("placed", true);
                //_animationDuration = _compressedBlock.clip.length;
            };
        }
        // Start is called before the first frame update
        void Start()
        {

            _target = new Vector3(_endPoint.x, _endPoint.y, _endPoint.z);


        }
        private void OnDestroy()
        {
            ParticleSystem tmp = Instantiate(_fx, transform.position, Quaternion.identity);
            tmp.Play();
 //           Destroy(tmp, tmp.main.duration);
        }
        // Update is called once per frame
        void Update()
        {
            if (!_math3.IsPlay)
            {
                return;
            }
            
            if (_move)
            {
                Vector3 _tmp = Vector2.MoveTowards(transform.position, _target, Time.deltaTime * _speed);
                _tmp.z = transform.position.z;
                transform.position = _tmp;
                if (Vector2.Distance(transform.position, _target) < 0.1)
                {
                    _move = false;
                    _target = _field.addToField2Random(gameObject);
                    GetComponent<BoxCollider2D>().enabled = false;
                    _fly = true;
                    return;
                }
            }
            
            if (_fly)
            {
              Vector3 _tmp =  Vector2.MoveTowards(transform.position, _target, Time.deltaTime * 1000);

                _tmp.z = transform.position.z;
                transform.position = _tmp;
                if (Vector2.Distance(transform.position, _target) < 0.1)
                {

                    //_field.addToField(gameObject);
                    blockStop();
 
                }
            }
          

        }

        private void OnMouseUp()
        {
            if (!_math3.IsPlay)
            {
                return;
            }

            // _field.addToField(gameObject);
            _target = _field.addToField2(gameObject);
//            _target = _field.GetCoords(gameObject);
            GetComponent<BoxCollider2D>().enabled = false;
            _move = false;
            _fly = true;
            Debug.Log(_target);
        }

        private  void OnMouseDrag()
        {
            if (!_math3.IsPlay)
            {
                return;
            }
            _move = false;
            Vector3 lastPos = transform.position;

            Vector3 tmp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            tmp.z = transform.position.z;

            if (tmp.x < _field.StartPos.x) 
            {
                tmp.x = _field.StartPos.x;
            };
                
                              
            if (tmp.x > _endPoint.x)
            {
                tmp.x = _endPoint.x;
            }

            if (tmp.y > _field.StartPos.y - _field.CellSize)
            {
                tmp.y = _field.StartPos.y-_field.CellSize ;
            }

            transform.position = tmp;
            /*
                        transform.position = tmp; bool flag = true;

                        if ( (transform.position.x < _field.StartPos.x) || (transform.position.x > _field.StartPos.x + (_field.Collumns * _field.CellSize) -_field.CellSize))
                        {
                            flag = false;
                        }

                        if (transform.position.y > (_field.StartPos.y - _field.CellSize * 1.5f))
                        {
                            flag = false;
                        } ;

                        if (!flag)
                        {
                            transform.position = lastPos;
                            return;
                        }
             */
        }
        #endregion
        #region public methods
        public void Placed()
        {
            _field.TestField();
        }
        #endregion
    }
}