﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Match3
{
    public class GaveOverMenu : MonoBehaviour
    {
        #region public methods
        public void Restart()
        {
            GameMath3.Instace().Main.Ads.ShowAds();
            Application.LoadLevel(Application.loadedLevel);
           
        }

        public void Exit()
        {
            GameMath3.Instace().Main.Ads.ShowAds();
            Application.Quit();
        }

        #endregion
    }
}
