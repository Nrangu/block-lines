﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Match3
{
    public class FadeText : MonoBehaviour
    {
        #region private fields
        Text _text;
        float _fadeSpeed = 1 / 2f;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            _text = GetComponent<Text>();
        }

        // Update is called once per frame
        void Update()
        {
            Color tmpColor = _text.color;
            tmpColor.a -= _fadeSpeed * Time.deltaTime;
            _text.color = tmpColor;
            if (tmpColor.a < 0)
            {
                Destroy(gameObject);
            }
        }
        #endregion
    }
}
