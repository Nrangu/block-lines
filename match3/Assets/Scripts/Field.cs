﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Match3
{

    public class Field : MonoBehaviour
    {
        #region private fields

        private GameMath3 _math3;
        [SerializeField]
        private GameObject _emptySell = null;
        [SerializeField]
        public int _colls;
        [SerializeField]
        public int _rows;
        [SerializeField]
        public Transform _startPos;
        [SerializeField]
        public float _cellSize;
        private GameObject[,] _field;

        private GameObject[,] _testField;
        private GameObject _currentObject = null;
        //private List<GameObject> _collector = new List<GameObject>();
        private List<CellObject> _collector = new List<CellObject>();
        private bool _updateField = false;
        #endregion
        #region inner class
        public class CellObject
        {
            public CellObject(GameObject obj_, int x_, int y_)
            {
                _obj = obj_;
                _x = x_;
                _y = y_;
            }
            public GameObject _obj;
            public int _x;
            public int _y;
        }
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            _math3 = GameMath3.Instace();
 
        }

        // Update is called once per frame
        private void LateUpdate()
        {
            if (_updateField)
            {
                _updateField = false;
                UpdateCells();
            }

        }
        void Update()
        {
        }

        void CopyField()
        {
            for (int x = 0; x < _colls; x++)
            {
                for (int y = 0; y < _rows; y++)
                {
                    _testField[x, y] = _field[x, y];
                }
            }
        }

        void TestCell(int x, int y)
        {
            if (_testField[x, y] == null)
            {
                return;
            }

            if (_currentObject == null)
            {
                _currentObject = _testField[x, y];
                _collector.Add(new CellObject( _testField[x, y],x,y ));

            }
            else
            {
                if (_currentObject.GetComponent<ColorBlock>()._type == _testField[x, y].GetComponent<ColorBlock>()._type)
                {

                    _collector.Add(new CellObject(_testField[x, y], x, y));
                } else
                {
                    return;
                }
            }
            _testField[x, y] = null;
            if (x + 1 < _colls)
            {
                TestCell(x + 1, y);
            }
            if (x - 1 >= 0)
            {
                TestCell(x - 1, y);
            }

            if (y + 1 < _rows)
            {
                TestCell(x, y + 1);
            }

            if (y - 1 >= 0)
            {
                TestCell(x, y - 1);
            }
        }
        private void UpdateCell(int x_)
        {
            for (int y = _rows-1; y >= 0; y--)
            {
                if (_field[x_, y] == null)
                {
                    if (!FindNotNull(x_, y))
                    {
                        continue;
                    }
                    _updateField = true;
                }
            }
            TestField();

        }

        private bool FindNotNull(int x_, int y_)
        {
            for (int y = y_; y >= 0; y--)
            {
                if (_field[x_, y] != null)
                {
                    _field[x_, y_] = _field[x_, y];
                    _field[x_, y] = null;
                    moveToField(_field[x_, y_],x_, y_);
                    return true;
                }
            }
            return false;
        }
        private void moveToField(GameObject object_, int coll_, int row_ )
        {
            float x = _startPos.position.x + coll_ * _cellSize;
            float y = _startPos.position.y + row_ * _cellSize;
            object_.transform.position = new Vector3(x, y, object_.transform.position.z);
        }
        #endregion
        #region public methods properties
        public void CreateField()
        {
            _startPos.position = new Vector3(_startPos.position.x + _cellSize / 2, _startPos.position.y, _startPos.position.z);
            _field = new GameObject[_colls, _rows];
            _testField = new GameObject[_colls, _rows];
            if (_emptySell != null)
            {
                float z = _emptySell.transform.position.z;
                for (int y = 0; y < _rows; y++)
                {
                    for (int x = 0; x < _colls; x++)
                    {
                        _field[x, y] = null;
                        GameObject tmp = Instantiate(_emptySell);
                        tmp.transform.position = new Vector3(_startPos.position.x + x * _cellSize, _startPos.position.y + y * _cellSize, z);
                        tmp.transform.localScale = new Vector3(_cellSize, _cellSize);
                    }
                }

            }
        }
        public Vector3 GetCoords(GameObject object_)
        {
            Vector3 pos = new Vector3();

            int sign = _startPos.position.x < 0 ? -1 : 1;
            float x = object_.transform.position.x + (_startPos.position.x * sign) + _cellSize / 2;
            int coll = (int)Math.Truncate(x / _cellSize);
            //x = _startPos.position.x + coll * _cellSize;

            int row = 0;
            for (int i = _rows - 1; i >= 0; i--)
            {
                if (_field[coll, i] == null)
                {
                    row = i;
                    break;
                }
            }

            pos.x = _startPos.position.x + coll * _cellSize;
            pos.y = _startPos.position.y + row * _cellSize;
            pos.z = object_.transform.position.z;
            return pos;
        }
        public Vector3 StartPos
        {
            get
            {
                return _startPos.position;
            }
        }
        public int Collumns
        {
            get
            {
                return _colls;
            }
        }

        public float CellSize
        {
            get
            {
                return _cellSize;
            }
        }
        public void addToField(GameObject object_)
        {
            int sign = _startPos.position.x < 0 ? -1 : 1;
            float x = object_.transform.position.x + (_startPos.position.x * sign) + _cellSize / 2;
            int coll = (int)Math.Truncate(x / _cellSize);
            //x = _startPos.position.x + coll * _cellSize;

            int row = 0;
            for (int i = _rows - 1; i >= 0; i--)
            {
                if (_field[coll, i] == null)
                {
                    row = i;
                    break;
                }
            }
            moveToField(object_, coll, row);
            //float y = _startPos.position.y + row * _cellSize;
            _field[coll, row] = object_;
            TestField();
            //object_.transform.position = new Vector3(x, y, object_.transform.position.z);

        }
        public Vector3 addToField2(GameObject object_)
        {
            Vector3 newPosition;
            int sign = _startPos.position.x < 0 ? -1 : 1;
            float x = object_.transform.position.x + (_startPos.position.x * sign) + _cellSize / 2;
            int coll = (int)Math.Truncate(x / _cellSize);
            //x = _startPos.position.x + coll * _cellSize;

            int row = -1;
            for (int i = _rows - 1; i >= 0; i--)
            {
                if (_field[coll, i] == null)
                {
                    row = i;
                    break;
                }
            }

             newPosition.x = _startPos.position.x + coll * _cellSize;
            newPosition.z = object_.transform.position.z;
            if (row == -1)
            {
                newPosition.y = _startPos.position.y + row * _cellSize;
                object_.GetComponent<ColorBlock>().blockStop += () => { _math3.GameOver(); };
                
               // Debug.Log("gameover");
            }
            else
            {
                newPosition.y = _startPos.position.y + row * _cellSize;
                _field[coll, row] = object_;
            }

            return newPosition;
//            moveToField(object_, coll, row);
            //float y = _startPos.position.y + row * _cellSize;
            TestField();
            //object_.transform.position = new Vector3(x, y, object_.transform.position.z);

        }

        public Vector3 addToField2Random(GameObject object_)
        {
            Vector3 newPosition;

            int coll = UnityEngine.Random.Range(0, _colls);

            //x = _startPos.position.x + coll * _cellSize;

            int row = -1;
            for (int i = _rows - 1; i >= 0; i--)
            {
                if (_field[coll, i] == null)
                {
                    row = i;
                    break;
                }
            }
            newPosition.x = _startPos.position.x + coll * _cellSize;
            newPosition.z = object_.transform.position.z; if (row == -1)
            {
                newPosition.y = _startPos.position.y + row * _cellSize;
                //_math3.GameOver();
                object_.GetComponent<ColorBlock>().blockStop += () => { _math3.GameOver(); };


            }
            else
            {
                newPosition.y = _startPos.position.y + row * _cellSize;
                _field[coll, row] = object_;
            }


            return newPosition;
            //            moveToField(object_, coll, row);
            //float y = _startPos.position.y + row * _cellSize;
            TestField();
            //object_.transform.position = new Vector3(x, y, object_.transform.position.z);

        }

        public void TestField()
        {

            CopyField();

            for (int x = 0; x < _colls; x++)
            {
                for (int y = 0; y < _rows; y++)
                {
                    _collector.Clear();
                    _currentObject = null;
                    TestCell(x, y);
                    if (_collector.Count > 2)
                    {
                         //Main.GetMain.AddScore(_collector.Count);
                        int score = _collector.Count;
/*
                        float minX = _collector[0].transform.position.x;
                        float maxX = _collector[0].transform.position.x;

                        float minY = _collector[0].transform.position.y;
                        float maxY = _collector[0].transform.position.y;
  */
                        float minX = _collector[0]._obj.transform.position.x;
                        float maxX = _collector[0]._obj.transform.position.x;

                        float minY = _collector[0]._obj.transform.position.y;
                        float maxY = _collector[0]._obj.transform.position.y;

                        foreach (CellObject obj in _collector)
                        {
                            if (minX > obj._obj.transform.position.x)
                            {
                                minX = obj._obj.transform.position.x;
                            }

                            if (maxX < obj._obj.transform.position.x)
                            {
                                maxX = obj._obj.transform.position.x;
                            }

                            if (minY > obj._obj.transform.position.y)
                            {
                                minY = obj._obj.transform.position.y;
                            }

                            if (maxY < obj._obj.transform.position.y)
                            {
                                maxY = obj._obj.transform.position.y;
                            }

                            _testField[obj._x, obj._y] = null;
                            _field[obj._x, obj._y] = null;
                            Destroy(obj._obj);
                        }
                        float xCoord = minX == maxX ? minX : (minX + maxX) / 2;
                        float yCoord = minY == maxY ? minY : (minY + maxY) / 2;


                        GameMath3.Instace().Main.AddScore(score, xCoord, yCoord);
                        _updateField = true;
                    }
                   
                }
            }

        }
        public void UpdateCells()
        {
            for (int x = 0; x < _colls; x++)
            {
                UpdateCell(x);
            }
        }
        #endregion
        //private int isCellNull(int x_)
        //{
        //    for(int y = 0; y < _rows; y++)
        //    {
        //        if(_field[x_,y] == null)
        //        {
        //            return y;
        //        }
        //    }
        //    return -1;
        //}
 
    }
}
