﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Advertising
{
    public class Ads
    {
        #region private fields
        string  _gameId = "123456";
        bool _testMode = true;
        string _placementId = "banner";
        #endregion
        #region public methods
        public Ads(string gameId_, bool testMode_)
        {
            _gameId = gameId_;
            _testMode = testMode_;
            Initialize();
        }
        public Ads()
        {
            Initialize();
        }
        public void Initialize()
        {
            Advertisement.Initialize(_gameId, _testMode);
        }
        public void ShowAds()
        {
            Advertisement.Show();
        }
        public IEnumerator ShowBannerWhenReady()
        {
            if (!_testMode)
            {
                while (!Advertisement.IsReady(_placementId))
                {
                    yield return new WaitForSeconds(0.5f);
                }
                Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
                Advertisement.Banner.Show(_placementId);
                yield return null;
            }
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
            Advertisement.Banner.Show(_placementId);



        }
        #endregion
        #region public properties
        public string GameId
        {
            get
            {
                return _gameId;
            }
            set
            {
                _gameId = value;
            }
        }
        public bool TestMode
        {
            get
            {
                return _testMode;

            }

            set
            {
                _testMode = value;
            }
        }

        public string PlacementId
        {
            get
            {
                return _placementId;
            }
            set
            {
                _placementId = value;
            }
        }
        #endregion
        #region private methods
  
        #endregion
    }
}
